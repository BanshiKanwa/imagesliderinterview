/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import HomeImageSlider from './src/screens/HomeImageSlider'
import { Provider } from 'react-redux';
import { configureStore, persistor } from './src/redux';
export const getStore = configureStore();
import { PersistGate } from 'redux-persist/lib/integration/react';
import Loader from './src/components/loading.components';
import { LogBox } from 'react-native';

class App extends React.Component {

  render()
  {
    LogBox.ignoreLogs(['Warning: ...']); 
    LogBox.ignoreAllLogs();
    return (
      <Provider store={getStore}>
        <PersistGate loading={<Loader loading={true} />} persistor={persistor}>
          <HomeImageSlider />
        </PersistGate>
      </Provider>
    );
  }
 
};


export default App;

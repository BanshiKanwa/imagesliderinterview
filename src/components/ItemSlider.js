import React, { PureComponent } from 'react';
import { View, Text } from 'react-native';
import FastImage from 'react-native-fast-image'
import { apiUrls } from "../services/apiUrls";
import { API_URL } from "../utils/restClient"
import styles from '../css/styles'
class ItemSlider extends PureComponent {

    render() {
        const {id,auther} = this.props; 
        return (<View style={styles.cardItem}>
            <FastImage
                style={styles.imageStyle}
                source={{
                    uri: `${API_URL}${apiUrls.photoUrl}${id}`,
                    priority: FastImage.priority.normal,
                }}
                resizeMode={FastImage.resizeMode.contain}
            />
            <Text style={styles.autherTitle}>{auther}</Text>
        </View>);
    }
}

export default ItemSlider;
import React from 'react';
import {ActivityIndicator, Modal, View} from 'react-native';
import styles from '../css/styles'

const Loader = props => {
  const {
    loading
  } = props;

  return (
    <Modal
      transparent={true}
      animationType={'none'}
      visible={loading}
      onRequestClose={() => {
      }}>
      <View style={styles.darkGray}>
        <View style={styles.activityIndicatorWrapper}>
          <ActivityIndicator
            animating={loading}/>
        </View>
      </View>
    </Modal>
  )
}

export default Loader;

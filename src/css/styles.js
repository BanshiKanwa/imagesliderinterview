import {StyleSheet} from 'react-native';
import {Color, shadow} from "../utils/ConstantColor";
 const styles = StyleSheet.create({
    modalBackground: {
      flex: 1,
      alignItems: 'center',
      flexDirection: 'column',
      justifyContent: 'space-around',
      backgroundColor: Color.black
    },
    activityIndicatorWrapper: {
      backgroundColor: Color.navigationBarColor,
      height: 100,
      width: 100,
      borderRadius: 10,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'space-around',
      ...shadow(4)
    },
    containerStyle:{ flex: 1 ,justifyContent:'center',alignItems:'center'},
    cardItem:{ height: 330, width: 202, backgroundColor: Color.gray, margin: 2,borderWidth:1,borderColor:Color.darkGray,borderRadius:5 },
    imageStyle:{ width: 200, height: 300 },
    appTitle:{color:Color.darkGray,fontSize:14,fontWeight:'bold',padding:14},
    autherTitle:{color:Color.white,padding:5,fontSize:12,fontWeight:'bold'}
  });

  export default styles;
import { createStore, applyMiddleware} from 'redux';
import rootReducer from './reducers';
import createSagaMiddleware from 'redux-saga';

import imageListSaga from './sagas/imageList.saga';
import { persistStore } from 'redux-persist';
import { composeWithDevTools } from 'redux-devtools-extension';
import monitorReducerEnhancer from './enhancers/monitorReducer';

const middlewares = [];

if (__DEV__) {
  const { createLogger } = require(`redux-logger`);
  const loggerMiddleware = createLogger();
  middlewares.push(loggerMiddleware);
}

export let persistor = '';

export const configureStore = persistedState => {
  const sagaMiddleware = createSagaMiddleware();
  middlewares.push(sagaMiddleware);

  const middlewareEnhancer = applyMiddleware(...middlewares);
  const enhancers = [middlewareEnhancer, monitorReducerEnhancer];
   const composedEnhancers = composeWithDevTools(...enhancers);
  const store = createStore(rootReducer, persistedState, composedEnhancers);

  sagaMiddleware.run(imageListSaga);
  persistor = persistStore(store);
  return store;
};

import {
  IMAGE_LIST_SUCCESS,
  CONNECTION_STATE_CHANGED,
  REFRESH_IMAGE_LIST_LOAD
} from '../actions/types';
const INITIAL_STATE = {
  isLoading: false,
  isConnected: true,
  isFetching:false,
 imageList:[],
};

const imageListReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case IMAGE_LIST_SUCCESS:
      return {
        ...state,
        imageList: action.payload.imageList,
        isFetching:false
      };
      case REFRESH_IMAGE_LIST_LOAD:
        return {
          ...state,
          isFetching:true
        };
      case CONNECTION_STATE_CHANGED:
        return { ...state, isConnected: action.payload };
    default:
      return state;
  }
};

export default imageListReducer;

import { combineReducers } from 'redux';
import imageListReducer from './imageListReducer';
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
const imagelistReducerPersistConfig = {
  key: 'imageListReducer',
  storage,
  blacklist: ['isLoading']
};

const rootPersistConfig = {
  key: 'root',
  version: 0, 
  storage,
  debug: true,
};

const rootReducer = combineReducers({
  imageListReducer: persistReducer(imagelistReducerPersistConfig, imageListReducer),
});

export default persistReducer(rootPersistConfig, rootReducer);

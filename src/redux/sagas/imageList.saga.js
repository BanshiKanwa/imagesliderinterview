import { call, put, takeLatest } from 'redux-saga/effects';
import {
  IMAGE_LIST,
  IMAGE_LIST_ERROR, IMAGE_LIST_SUCCESS,
  IMAGE_LIST_LOADING,
  REFRESH_IMAGE_LIST_LOAD
} from '../actions/types';
import {
  getImageList
} from '../../services/HomeImageSlider';
import { showToast } from '../../utils/toast';
import * as messages from '../../utils/messages';
import { regex } from '../../utils/regex';

function* getImageSaga(action) {
  try {
    regex.showLoader();
    const {isRefresh} = action.payload
    if (isRefresh) {
      yield put({ type: REFRESH_IMAGE_LIST_LOAD });
    }
    else {
      yield put({ type: IMAGE_LIST_LOADING });
    }

    const { json } = yield call(getImageList);

    if (isRefresh) {
      let i = json.length - 1;
      for (; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        const temp = json[i];
        json[i] = json[j];
        json[j] = temp;
      }
    }

    regex.hideLoader();
    showToast(messages.imageMsg)
    yield put({ type: IMAGE_LIST_SUCCESS, payload: { imageList: json } });
  } catch (e) {

    regex.hideLoader();
    showToast(messages.imageMsgError)
    console.log('getImageSaga error', e);
    yield put({ type: IMAGE_LIST_ERROR });
  }
}

export default function* imageListSaga() {
  yield takeLatest(IMAGE_LIST, getImageSaga);
}

import { IMAGE_LIST } from "../../redux/actions/types";

export const getImageAction = (isRefresh) => {
  return {
    type: IMAGE_LIST,
    payload:{isRefresh:isRefresh}
  };
};
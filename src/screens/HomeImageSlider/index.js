import React, { Component } from 'react';
import { FlatList, View, Text } from 'react-native';
import { getImageAction } from './homeImageSliderAction'
import ItemSlider from '../../components/ItemSlider'
import { connect } from "react-redux";
import styles from '../../css/styles'

class HomeImageSlider extends Component {

  componentDidMount() {
    this.props.getImageAction();
  }
  _onImageRefresh = () => {
    this.props.getImageAction(true);
  }
  render() {
    return (
      <View style={styles.containerStyle}>
        <Text style={styles.appTitle}>Image Slider</Text>
        {this.props.imageList.length != 0 && (<FlatList
          data={this.props.imageList}
          keyExtractor={item => item.id.toString()}
          horizontal={true}
          onRefresh={() => this._onImageRefresh()}
          showsHorizontalScrollIndicator={false}
          refreshing={this.props.isFetching}
          renderItem={({ item }) =>
            <ItemSlider auther={item.author} id={item.id}
            />
          }
        />)}
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    isFetching: state.imageListReducer.isFetching,
    imageList: state.imageListReducer.imageList,
  };
};

export default connect(mapStateToProps, { getImageAction })(HomeImageSlider)


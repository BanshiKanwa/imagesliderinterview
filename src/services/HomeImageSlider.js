import {defaultRestClient} from "../utils/restClient";
import {apiUrls} from "./apiUrls";

export const getImageList = () => {
  return defaultRestClient.get(apiUrls.imagelist)
};
import {Platform} from 'react-native';

export const Color = {
  navigationBarColor: '#282828',
  white: '#FFF',
  black: '#000',
  gray:'#808080',
  darkGray:'#242424'
};
const constants = {
  black:Color.black,
};

export const shadow = (elevation, spread = 5, offsetX = 0, offsetY = 0) => Platform.select({
  ios: {
    shadowOffset: {
      width: offsetX,
      height: offsetY
    },
    shadowOpacity: 0.5,
    shadowRadius: spread,
    shadowColor: constants.black
  },
  android: {
    elevation: elevation
  }
});

export default constants;
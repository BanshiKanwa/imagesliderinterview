'use strict';

import { HIDE_LOADER, SHOW_LOADER } from "../redux/actions/types";
import { getStore } from '../../App';


export const regex = {
  showLoader: () => {
    getStore.dispatch({
      type: SHOW_LOADER
    })
  },

  hideLoader: () => {
    getStore.dispatch({
      type: HIDE_LOADER
    })
  },

};

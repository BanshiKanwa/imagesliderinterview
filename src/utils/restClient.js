
export const successCode = 1;
const fetchRetry = (url, delay, limit, fetchOptions = {}) => {
  return new Promise((resolve, reject) => {
    function success(response) {
      resolve(response);
    }
    function failure(error) {
      limit--;
      if (limit) {
        setTimeout(fetchUrl, delay);
      } else {
        // this time it failed for real
        reject(error);
      }
    }
    function fetchUrl() {
      return fetch(url, fetchOptions)
        .then(success)
        .catch(failure);
    }
    fetchUrl();
  });
};

export class RestClient {
  constructor(baseUrl) {
    this._baseUrl = baseUrl;
  }
 
  _callWithoutBody(method, url) {
    let init = {
      method: method,
      headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data'
      },
      cache: 'no-cache',
      keepalive: false,
      redirect: 'follow'
    };
    return this.callRequest(init, url);
  }
  callRequest(init, url) {
    let APIURL = `${this._baseUrl}/${url}`;

    console.log(`[restClient]: The base url is: ${this._baseUrl} and url is: ${url}`);


      return fetchRetry(APIURL, 300, 3, init)
          .then(response => {
            return response.json().then(json => {
              console.log({
                // DO NOT USE THIS TYPE FOR REDUCERS. JUST FOR DEBUGGING ONLY
                type: 'REST_API_RESPONSE_RECEIVED',
                payload: {
                  url: APIURL,
                  type: response.type,
                  status: response.response,
                  ok: response.ok,
                  response: json
                }
              });
              return Promise.resolve({json})
            
            });
          })
          .catch(error => {
            console.log({
              // DO NOT USE THIS TYPE FOR REDUCERS. JUST FOR DEBUGGING ONLY
              type: 'REST_API_ERROR_RECEIVED',
              payload: {
                url: APIURL,
                error,
                message: error.message
              }
            });
            return Promise.resolve({json:error});
          });
  }

  get(url) {
    return this._callWithoutBody('GET', url);
  }
}

const PRODUCTION_URL = 'https://picsum.photos/';

export const API_URL = PRODUCTION_URL;

export const defaultRestClient = new RestClient(API_URL);
import Toast from 'react-native-root-toast';
import {Color} from "../utils/ConstantColor";

export const showToast = (msg) => {
return Toast.show(msg, {
    duration: Toast.durations.LONG,
    position: Toast.positions.BOTTOM,
    shadow: true,
    animation: true,
    hideOnPress: true,
    delay: 0,
    backgroundColor: Color.navigationBarColor,
    textColor: Color.white
  });
};
